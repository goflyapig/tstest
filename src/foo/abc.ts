import { Zed } from './zed'

export class Abc {
    zed: Zed;

    constructor() {
        this.zed = new Zed();
    }

    doIt(): void {
        this.zed.write("abc");
    }
}