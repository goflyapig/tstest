import doublePlusOne from './sub/doublePlusOne';
import $ from 'jquery';

class Bar {
    constructor(){}
    doIt() {
        $('#message').html(`<p>double 5 plus one: ${doublePlusOne(5)}</p>`);
    }
}

new Bar().doIt();