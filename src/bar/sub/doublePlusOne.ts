import double from '../double';

export default function doublePlusOne(x: number): number {
    return double(x) + 1;
}