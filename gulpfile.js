/// <reference path="./node_modules/@types/node/index.d.ts" />

var gulp = require('gulp');
var ts = require('gulp-typescript');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var named = require('vinyl-named');
var rollup = require('rollup');
var rollupSourcemaps = require('rollup-plugin-sourcemaps');
var fs = require('fs');
var del = require('del');
var path = require('path');

var tsProject = ts.createProject('tsconfig.json');

var paths = {
  src: 'src',
  lib: 'lib',
  build: 'build',
  intermediate: 'build/intermediate',
  dist: 'build/dist',
  distBundles: 'build/dist/bundles',
  distLib: 'build/dist/lib'
};

gulp.task('lib', function() {
  return gulp.src(`${paths.lib}/**/*.js`)
    .pipe(gulp.dest(paths.distLib));
});

gulp.task('index', function() {
  return gulp.src(`${paths.src}/index.html`)
    .pipe(gulp.dest(paths.dist));
});

gulp.task('tsc', function() {
  return gulp.src(`${paths.src}/**/*.ts`)
    .pipe(sourcemaps.init())
    .pipe(tsProject())
    .pipe(sourcemaps.write('.', {
      mapSources: sourcePath => path.join(__dirname, paths.src, sourcePath)
    }))
    .pipe(gulp.dest(paths.intermediate));
});

gulp.task('bundle-js', ['tsc'], function() {
  var features = fs.readdirSync(paths.intermediate)
    .map(name => ({ name: name, path: path.join(paths.intermediate, name) }))
    .filter(feature => fs.statSync(feature.path).isDirectory());

  return Promise.all(features.map(feature => {
    return rollup.rollup({
      entry: path.join(__dirname, feature.path, `${feature.name}.js`),
      external: function(importee) {
        if (path.isAbsolute(importee)) {
          var pathRelativeToBundleFolder = path.relative(feature.path, importee);
          var isOutsideFolder = pathRelativeToBundleFolder.indexOf('..') === 0;
          return isOutsideFolder;
        } else {
          var isRelativeImport = importee.indexOf('.') === 0;
          return !isRelativeImport;
        }
      },
      plugins: [rollupSourcemaps()]
    }).then(function(bundle) {
      return bundle.write({
        format: 'amd',
        dest: path.join(__dirname, paths.distBundles, `${feature.name}/${feature.name}.js`),
        sourceMap: true
      });
    });
  }));
});

gulp.task('bundle-tsd', ['tsc'], function() {
  var tsdFiles = fs.readdirSync(paths.intermediate)
    .map(name => ({ name: name, path: path.join(paths.intermediate, name) }))
    .filter(feature => fs.statSync(feature.path).isDirectory())
    .map(feature => `${feature.path}/${feature.name}.d.ts`);
  
  return gulp.src(tsdFiles, { base: paths.intermediate })
    .pipe(gulp.dest(paths.distBundles));
});

gulp.task('bundles', ['bundle-js', 'bundle-tsd']);

gulp.task('incrBuild', ['lib', 'index', 'bundles']);

gulp.task('clean', function() {
  return del([paths.build]);
});

gulp.task('build', ['clean'], function() {
  return gulp.start('incrBuild');
});

gulp.task('watch', function() {
  gulp.watch(path.join(paths.src, '**/*.ts'), ['build']);
});
